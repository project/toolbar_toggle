(() => {
  const key = "Drupal.toolbar_toggle.isToolbarHidden";
  const isToolbarHidden = localStorage.getItem(key);
  document.body.classList.toggle("toolbar-hidden", isToolbarHidden === "true");
  document.addEventListener("keydown", (event) => {
    if (event.key === "F4") {
      const isToolbarHidden = document.body.classList.toggle("toolbar-hidden");
      localStorage.setItem(key, isToolbarHidden);
    }
  });
})();
